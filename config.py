white_cards = {
    "W000": "Cygańska klątwa.",
    "W001": "Minuta ciszy.",
    "W002": "Gej party.",
    "W003": "Uczciwy gliniarz z niczym do stracenia.",
    "W004": "Głód.",
    "W005": "Bakteria żywiąca się mięsem.",
    "W006": "Olewanie problemów trzeciego świata.",
    "W007": "Sprośne SMSy.",
    "W008": "Gwiazdy porno.",
    "W009": "Mordowanie, palenie, gwałcenie.",
    "W010": "72 dziewice.",
    "W011": "Napad w bramie.",
    "W012": "Paradoks podróży w czasie.",
    "W013": "Złoty łańcuch.",
    "W014": "Konsultanci.",
    "W015": "Dług publiczny.",
    "W016": "Zrzucane żyrandolu na przeciwników i wjeżdżanie do góry po linie.",
    "W017": "Jarosław Kaczyński.",
    "W018": "Nagość.",
    "W019": "Terapia hormonalna.",
    "W020": "Rozbieranie się do naga i oglądanie Cartoon Network.",
    "W021": "Udawane współczucie.",
    "W022": "Publiczne ośmieszenie.",
    "W023": "Używanie wspólnej strykawki.",
    "W024": "Baby z nosa.",
    "W025": "Cud porodu.",
    "W026": "Obowiązki małżeńskie.",
    "W027": "Dezodorant AXE.",
    "W028": "Krew Chrystusa.",
    "W029": "Przerażające wypadki podczas laserowego usuwania włosów.",
    "W030": "BATMAN!!!.",
    "W031": "Rolnictwo.",
    "W032": "Naturalna selekcja.",
    "W033": "Amatorskie aborcje.",
    "W034": "World of Warcraft.",
    "W035": "Otyłość.",
    "W036": "Homoerotyczna scena gry w siatkówkę.",
    "W037": "Szczękościsk.",
    "W038": "Taniec godowy.",
    "W039": "Skręt jądra.",
    "W040": "Hot-dogi za 1zł.",
    "W041": "Tede.",
    "W042": "Ser topiony.",
    "W043": "Ataki dinozaurów.",
    "W044": "Ściąganie koszulki.",
    "W045": '"Serek" spod napletka',
    "W046": "Alkoholizm.",
    "W047": "40-latek na rolkach.",
    "W048": "Obżeranie się i wymiotywanie.",
    "W049": "Olbrzymie laski.",
    "W050": "Wstręt do samego siebie.",
    "W051": "Dzieci na smyczy.",
    "W052": "Żałosna gra wstępna.",
    "W053": "Biblia.",
    "W054": "Niemieckie porno w lochach.",
    "W055": "Palenie się.",
    "W056": "Nastoletnie ciąże.",
    "W057": "Gandhi.",
    "W058": "Zostawianie kłopotliwych wiadomości na poczcie głosowej.",
    "W059": "Cios podbródkowy.",
    "W060": "Paracownicy działu obsługi klienta.",
    "W061": "Wzwód trwający ponad 4 godziny.",
    "W062": "Moje genitalia.",
    "W063": "Podrywanie dziewczyn w klinice aborcyjnej.",
    "W064": "Nauka.",
    "W065": "Nieodwzajemniony seks oralny.",
    "W066": "Ptaki nieloty.",
    "W067": "Mocne sztachnięcie.",
    "W068": "Tortury.",
    "W069": "Zbilansowane śniadanie.",
    "W070": "Faktyczne zabranie dziecku cukierka.",
    "W071": "Fundacja Polsat dzieciom.",
    "W072": "Potajemne drapanie się po tyłku.",
    "W073": "Pasywno-agresyne ogłoszenia.",
    "W074": "Chińska drużyna gimnastyczna.",
    "W075": "Popuszczenie w majtki.",
    "W076": "Zmazy nocne.",
    "W077": "Żydzi.",
    "W078": "Potężne uda.",
    "W079": "Puszczanie oczek do starszych ludzi.",
    "W080": "Delikatne pieszczoty wewnętrznej części uda.",
    "W081": "Napięcie seksualne.",
    "W082": "Zakazany owoc.",
    "W083": "Szkieletor.",
    "W084": "Whiskas®.",
    "W085": "Bogactwo.",
    "W086": "Słodka zemsta.",
    "W087": "PiS.",
    "W088": "Anna Mucha.",
    "W089": "Macanie.",
    "W090": "Piloci kamikaze.",
    "W091": "Andrzej Grabowski.",
    "W092": "Propaganda homoseksualna.",
    "W093": "Ciężko pracujący cygan.",
    "W094": "Sokół z czapką na głowie.",
    "W095": "Ministranci.",
    "W096": "Bycie tak złym, że aż ci staje.",
    "W097": "darmowe próbki.",
    "W098": "Wielkie halo o nic.",
    "W099": "Właściwe postępowanie.",
    "W100": "Laktacja.",
    "W101": "Pokój na świecie.",
    "W102": "RoboCop.",
    "W103": "Bezczelność.",
    "W104": "Justin Bieber.",
    "W105": "Krasnoludki.",
    "W106": "Niestosowne jodłowanie.",
    "W107": "Dojrzewanie płuciowe.",
    "W108": "Duchy.",
    "W109": "Niesymetryczne sztuczne piersi.",
    "W110": "Palcówka.",
    "W111": "Kuba Wojewódzki zaczepiony o haczyk od zasłony.",
    "W112": "Mleko w tubce.",
    "W113": "Policyjna przemoc.",
    "W114": "General Wojciech Jaruzelski.",
    "W115": "Dzieci.",
    "W116": "Skalpowanie.",
    "W117": "Smsowanie.",
    "W118": "Darth Vader.",
    "W119": "Dokładnie to, czego się spodziewasz.",
    "W120": "To uczucie, gdy wydaje ci się, że to tylko beknięcie, a okazuje się, że to rzyg.",
    "W121": "Leki na ADHD.",
    "W122": "Komórki macierzyste.",
    "W123": "Lobotomia szpikulcem do lodu.",
    "W124": "Borys Szyc.",
    "W125": "Opryszczka.",
    "W126": "Kaszaloty.",
    "W127": "Bezdomni.",
    "W128": "Petting.",
    "W129": "Kazirodstwo.",
    "W130": "Pac-man tryskający spermą w amoku.",
    "W131": "Mim w trakcie zawału.",
    "W132": "Robert Burneika.",
    "W133": "Bóg.",
    "W134": "Złoty deszcz.",
    "W135": "Emocje.",
    "W136": "Oblizywanie rzeczy, żeby oznaczyć ich jako swoje.",
    "W137": "Tyskie.",
    "W138": "Łożysko.",
    "W139": "Spontaniczne samospalenie.",
    "W140": "Przyjaciele z bonusem.",
    "W141": "Malowanie palcami.",
    "W142": "Zapach starych ludzi.",
    "W143": "Moje wewnętrzne demony.",
    "W144": "Gąbka pełna kocich sików.",
    "W145": "Józef Piłsudski.",
    "W146": "Przytulanie.",
    "W147": "Zielsko / Jaranie / Palenie / Trawa / Ganja.",
    "W148": "Walki kogutów.",
    "W149": "Strzelanie po swoich.",
    "W150": "Lech Wałęsa.",
    "W151": "Rozczarowujące przyjęcie urodzinowe.",
    "W152": "Finalista kangura.",
    "W153": "Kucyk.",
    "W154": "Olaf Lubaszenko.",
    "W155": "Wyrusznie w stronę zachodu słońca.",
    "W156": "Nagły zwrot akcji.",
    "W157": "Pedofile.",
    "W158": "Drożdże.",
    "W159": "Okradanie grobów.",
    "W160": "Katapulty.",
    "W161": "Biedni ludzie.",
    "W162": "Moc Jedi.",
    "W163": "Paplanie.",
    "W164": "AIDS.",
    "W165": "Zdjęcia piersi.",
    "W166": "Nelli Rokita.",
    "W167": "Amerykańscy gladiatorzy.",
    "W168": "Bycie naprawdę mocno spalonym.",
    "W169": "Scjentologia.",
    "W170": "Leczenie homoseksualizmu.",
    "W171": "Igraszki.",
    "W172": "Dwóch karłów spadających do wiadra.",
    "W173": "Ku Klux Klan.",
    "W174": "Czyngis-chan.",
    "W175": "Metamfetamina.",
    "W176": "Poddaństwo.",
    "W177": "Nierozmawianie z obcymi.",
    "W178": "Kariera aktorska Przemysława Salety.",
    "W179": "Hasanie.",
    "W180": "Samosąd.",
    "W181": "Ocenzurowane bukkake.",
    "W182": "Wieczny smutek.",
    "W183": "Rasizm.",
    "W184": "Rzut karłem.",
    "W185": "Bezchmurne niebo i świecące słońce.",
    "W186": "Małpa paląca cygara.",
    "W187": "Gwałtowna powódź.",
    "W188": "Suche wymioty.",
    "W189": "Terroryści.",
    "W190": "Britney Spers w wieku 55 lat.",
    "W191": "Nastawienie.",
    "W192": "Spontanicznie wyrwanie się do tańca i śpiewu.",
    "W193": "Trąd.",
    "W194": "Kolczyki do sutków.",
    "W195": "Serce dziecka.",
    "W196": "Szczeniaczki!.",
    "W197": "Pobudka na parkingu będąc na wpół nagim.",
    "W198": "Pochwa Marii Czubaszek.",
    "W199": "To miejsce u mężczyzny między jądrami a odbytem.",
    "W200": "Aktywne słuchanie.",
    "W201": "Czystki etniczne.",
    "W202": "Parowóz Tomek.",
    "W203": "Czekanie do ślubu.",
    "W204": "Niewyobrażalna głupota.",
    "W205": "Euphoria™ Calvina Kleina.",
    "W206": "Przekazywanie dalej niechcianych prezentów.",
    "W207": "Autokanibalizm.",
    "W208": "Problemy z erekcją.",
    "W209": "Moja kolekcja nowoczesnych gadżetów erotycznych.",
    "W210": "Papież.",
    "W211": "Biali ludzie.",
    "W212": "Porno z mackami.",
    "W213": "Konwulsyjnie wymiotujący Kuba Wojewódzki podczas gdy chmara pająków wylęga się w jego mózgu i eksploduje z kanałów łzowych.",
    "W214": "Za dużo żelu na włosach.",
    "W215": "Seppuku.",
    "W216": "Oszukiwanie na olimpiadzie specjalnej.",
    "W217": "Charyzma.",
    "W218": "Tomasz Karolak.",
    "W219": "Tomasz Kot.",
    "W220": "Zespół Feel.",
    "W221": "Zerknięcie.",
    "W222": "Sranie tam i z powrotem. W nieskończoność.",
    "W223": "Menstruacja.",
    "W224": "Dzieci z rakiem odbytu.",
    "W225": "Śląsk.",
    "W226": "Naruszanie podstawowych praw człowieka.",
    "W227": "Gwałt na randce.",
    "W228": "Bycie bajecznie pięknym.",
    "W229": "Nekrofilia.",
    "W230": "Centaury.",
    "W231": "Adam Słodowy.",
    "W232": "Murzyni.",
    "W233": "Rycerskość.",
    "W234": "Suki.",
    "W235": "Głęboko upośledzeni.",
    "W236": "Chwytające za serce sierotki.",
    "W237": "MechaHitler.",
    "W238": "Pieczenie odbytu.",
    "W239": "Kolejny, cholerny film z wampirami.",
    "W240": "Prawdziwe znaczenie świąt.",
    "W241": "Estrogen.",
    "W242": "Ten pas na brzuch z TV Mango.",
    "W243": "Rodzenie kamienia nerkowego.",
    "W244": "Wybielony odbyt.",
    "W245": "Michael Jackson.",
    "W246": "Cybernetyczne ulepszenia.",
    "W247": "Kolesie, którzy nie dzwonią później do dziewczyn poznanych na imprezie.",
    "W248": "Masturbacja.",
    "W249": "Pierdzenie pochwą.",
    "W250": "Ukrywanie wzwodu.",
    "W251": "Jadalna bielizna.",
    "W252": "Viagra®.",
    "W253": "Za gorąca dupa.",
    "W254": "Mahomet (chwała mu po wieczne czasy).",
    "W255": "Seks niespodzianka!.",
    "W256": "Picie w samotności.",
    "W257": "Dziurawe ręce.",
    "W258": "Liczne rany kłute.",
    "W259": "Ubrudzenie się.",
    "W260": "Wykorzystywanie dzieci.",
    "W261": "Koraliki analne.",
    "W262": "Ofiary wśród cywili.",
    "W263": "Piotr Adamczyk.",
    "W264": "Konina.",
    "W265": "Naprawdę fajny kapelusz.",
    "W266": "Kim Jong-il.",
    "W267": "Zabłąkany włos łonowy.",
    "W268": "Kochanie się w tyłek.",
    "W269": "Dokarmianie Magdy Gessler.",
    "W270": "Uczenie robota miłości.",
    "W271": "Wpierdol.",
    "W272": "Młyn pełen trupów.",
    "W273": "Chocapic.",
    "W274": "Noszenie bielizny na lewą stronę by uniknąć prania.",
    "W275": "Śmiercionośny laser.",
    "W276": "Zamrażarka pełna organów.",
    "W277": "Amerykański sen.",
    "W278": "Mokry bąk.",
    "W279": "Martwe niemowlęta.",
    "W280": "Napletek.",
    "W281": "Solówki na saksofonie.",
    "W282": "Niemcy.",
    "W283": "Płód.",
    "W284": "Janusz Korwin-Mikke.",
    "W285": "Amputanci.",
    "W286": "Status moje związku.",
    "W287": "Olaf Lubaszenko.",
    "W288": "Pszczoły?",
    "W289": "Erotyczna wersja Harrego Pottera.",
    "W290": "Studia.",
    "W291": "Upijanie się płynem do spryskiwaczy.",
    "W292": "Naziści.",
    "W293": "Zażywanie Krokodyla.",
    "W294": "Steven Hawking mówiący świństwa.",
    "W295": "Martwi rodzice.",
    "W296": "Przeciwstawne kciuki.",
    "W297": "Gadanie głupot.",
    "W298": "Piły łańcuchowe zamiast rąk.",
    "W299": "Cezary Pazura.",
    "W300": "Dziecięce konkursy piękności.",
    "W301": "Wybuchy.",
    "W302": "Wąchanie kleju.",
    "W303": "Kuba Wojewódzki nękany przez rój myszołowów.",
    "W304": "Tabletki gwałtu.",
    "W305": "Moja pochwa.",
    "W306": "Spodnie bez tyłka.",
    "W307": "Dawanie z siebie 110%.",
    "W308": "Jej wysokość, królowa Elżbieta II.",
    "W309": "Bycie zmarginalizowanym.",
    "W310": "Gobliny.",
    "W311": "Nadzieja.",
    "W312": "Mikro penis.",
    "W313": "Moja dusza.",
    "W314": "Wikingowie.",
    "W315": "Piękni ludzie.",
    "W316": "Uwodzenie.",
    "W317": "Kompleks Edypa.",
    "W318": "Gęsi.",
    "W319": "Globalne ocieplenie.",
    "W320": "Robienie smutnej minki.",
    "W321": "Kobiece prawa wyborcze.",
    "W322": "Dziurawa prezerwatywa.",
    "W323": "Sędzia Anna Maria Wesołowska.",
    "W324": "Afrykańskie dzieci.",
    "W325": "Donald Tusk.",
    "W326": "Wymiana urzejmości.",
    "W327": "Zabawa w wampira podczas miesiączki.",
    "W328": "Arnold Schwarzenegger.",
    "W329": "Obciąganie w czasie jazdy autem.",
    "W330": "Spektakularne mięśnie brzucha.",
    "W331": "Mazurek.",
    "W332": "Zaspany Lew w ZOO.",
    "W333": "Paczka magicznych fasolek.",
    "W334": "Złe decyzje życiowe.",
    "W335": "Moje życie seksualne.",
    "W336": "Auschwitz.",
    "W337": "Żółw jaszczurowaty gryzący się w końcówkę penisa.",
    "W338": "Wybuch termojądrowy.",
    "W339": "Łechtaczka.",
    "W340": "Wielki wybuch.",
    "W341": "Miny lądowe.",
    "W342": "Znajomi wyjadający całą zagrychę.",
    "W343": "Kozy pożerające śmieci.",
    "W344": "Jezioro łabędzie.",
    "W345": "Spuszczanie się do basenu dziecięcych łez.",
    "W346": "Męska Pała.",
    "W347": "Czas dla siebie.",
    "W348": "Opowiadanie dowcipów o żydach w nieodpowiednim momencie.",
    "W349": "Morze problemów.",
    "W350": "Fantazje o drwalach.",
    "W351": "Głos Krystyny Czubówny.",
    "W352": "Kobiety w reklamach jogurtów.",
    "W353": "Ziołowe tabletki na problemy z potencją.",
    "W354": "Bycie pierdolonym czarodziejem.",
    "W355": "Piercing genitaliów.",
    "W356": "Znośni transwestyci.",
    "W357": "Seksowne walki na poduszki.",
    "W358": "Jądra.",
    "W359": "Babcia.",
    "W360": "Tarcie.",
    "W361": "Ludzie zawsze psujący dobrą zabawę.",
    "W362": "Pierdzenie i odchodzenie.",
    "W363": "Bycie chujem w stosunku do dzieci.",
    "W364": "Zastawianie pułapek na złodziei.",
    "W365": "Materac z TV Mango.",
    "W366": "Umieranie.",
    "W367": "Huragan Katrina.",
    "W368": "Geje.",
    "W379": "Ludska głupota.",
    "W380": "Mężczyźni.",
    "W381": "Amisze.",
    "W382": "Pterodaktyle jaja.",
    "W383": "Zajęcia integracyjne.",
    "W384": "Guz mózgu.",
    "W385": "Karty przeciwko ludzkości.",
    "W386": "Strach w czystej postaci.",
    "W387": "Lady Gaga.",
    "W388": "Mleczarz.",
    "W389": "Niewyparzona buzia."
}

black_cards = {

    "B000": "Podczas lotu samolotem zabrania się {}.",
    "B001": "To straszna szkoda, że wszystkie dzieci w dzisiejszych czasach mają do czynienia z {}.",
    "B002": "Za 1000 lat zamiast papierkowych pieniędzy będziemy używać {}.",
    "B003": "PZPN zakazał {}, bowiem dawało to graczom niesprawidliwą przewagę.",
    "B004": "Czego wstydzi się Batman?",
    "B005": "Następna książka JK Rowling będzie nosiła tytuł: Harry Potter i Komnata {}.",
    "B006": "Co przywiozłem z Turcji?",
    "B007": "{}? Istnieje aplikacja, która to robi.",
    "B008": "Co jest moim narkotykiem?",
    "B009": "Gdy USA ściągało się ze Związkiem Radzieckim na księżyc, rząd Meksyku wydał milion pesos na {}.",
    "B010": "W nowym filmie Disneya Hannah Montana po raz pierwszy zmaga się z {}.",
    "B011": "Co jest moją supermocą?",
    "B012": "Co jest kolejną modną dietą?",
    "B013": "Co Bogusław Linda jadł na obiad?",
    "B014": "Gdy Faraon nie dawał się przkonać Mojżesz zesłał na plagę {}.",
    "B015": "W jaki sposób udaje mi się utrzymać mój związek?",
    "B016": "Co jest najbardziej churupiące?",
    "B017": "W więzieniu za 200 fajek dostaniesz {}.",
    "B018": "Po wielkiej powodzi w 2010 roku, jako pomoc, polski rząd zorganizował powodzianom {}.",
    "B019": "W dzisiejszych czasach niegrzeczne dzieci zamiast rozgi dostają pod choinkę {}",
    "B020": "Życie Indian zostało na zawsze zmienione gdy biały człowiek pokazał im {}.",
    "B021": "Przy pomocy czego polski rząd wspiera studentów z regionów wiejskich?",
    "B022": "Może to jej urok a może to {}.",
    "B023": "W ostatnich chwilach swojego życia Michael Jackson myślał o {}.",
    "B024": "Biali ludzie lubią {}.",
    "B025": "Przez co wszytsko mnie boli?",
    "B026": "Romantyczna kolacja ze świecami nie może obejść się bez {}.",
    "B027": "Co sprowadzę z przyszłości by przkonać ludzi, że jestem potężnym czarodziejem.",
    "B028": "TV Mango prezentuje {}.",
    "B029": "Wycieczka szkolna została zrujnowana przez {}.",
    "B030": "Co jest najlepszym przyjacielem kobiety?",
    "B031": "Drogie Bravo, mam problem z {}.",
    "B032": "Jak będę premierem, to stworzę Ministerstwo {}.",
    "B033": "Co ukrywają przede mną rodzice?",
    "B034": "Co zawsze rozkręca imprezy?",
    "B035": "Co staje się lepsze z wiekiem?",
    "B036": "{} dobry do ostatniej kropli.",
    "B037": "{}. To pułapka!",
    "B038": "W nowym reality show MTV ośmiu celebrytów wprowadza się do domu z {}.",
    "B039": "Co jest najbardziej emo?",
    "B040": "W czasie seksu lubię myśleć o {}.",
    "B041": "Co zakończyło mój ostatni związek?",
    "B042": "Co to za dżwięk?",
    "B043": "{}. Tak chcę umrzeć.",
    "B044": "Przez co cały się kleje?",
    "B045": "Co będzie następną zabawką w zestawie Happy Meal®?",
    "B046": "Czego jest pełno w niebie?",
    "B047": "Nie wiem czym będzie się walczyć podczas trzeciej wojny światowej, ale podczas czwartej wojny światowej będzie się walczyć {}.",
    "B048": "Na co zawsze można poderwać laskę?",
    "B049": "{}: Dla ciebie, dla Rodziny.",
    "B050": "Przez co nie mogę spać w nocy?",
    "B051": "Co tak pachnie?",
    "B052": "Co pomaga Obamie wyluzować?",
    "B053": "Do teatrów wchodzi musical o {}.",
    "B054": "Antropologowie odkryli ostatnio starożytne plemie czczące {}.",
    "B055": "Zanim Pana zabije, Panie Bond, muszę pokazać Panu {}.",
    "B056": "Badania wykazują, że myszy laboratoryjne pokonują labirynt 50% szybciej po kontakcie z {}.",
    "B057": "Na żywo, tylko w Polsat Sport: cykl turnejów {}.",
    "B058": "Jak będę miliarderem, to wzniosę 50 metrowy pomnik na cześć {}.",
    "B059": "By przyciągać więcej gości, Muzeum Narodowe otworzyło interaktywny eksponat z {}.",
    "B060": "Wojna! Jaki z niej pożytek?",
    "B061": "Co powoduje u mnie gazy?",
    "B062": "Czym śmierdzą stare ludzie?",
    "B063": "Z czego rezygnuje w czasie postu?",
    "B064": "Medycyna alternatywna zaczęła doceniać właściwości lecznicze {}.",
    "B065": "Co USA wysyłało dzieciom w Afganistanie?",
    "B066": "Co preferuje Janusz Korwin-Mikke?",
    "B067": "Czego nie chcielibyście znaleźć w chińskim żarciu?",
    "B068": "Pije by zapomnieć o {}.",
    "B069": "{}. Piątka stary!",
    "B070": "Picasso w czasie swoich twórczych eksperymentów z uproszczeniem formy stworzył setki obrazów przedstawiających {}.",
    "B071": "Przeprawszam profesorze, ale nie mogłem odrobić pracy domowej przez {}.",
    "B072": "Co będzie następnycm duetem superbohaterów?",
    "B073": "{} to pierwszy krok do {}.",
    "B074": "Tak, to ja zabiłem {}. Pytacie jak? {}.",
    "B075": "Nagrodę za {} otzrymuje {}.",
    "B076": "Jako moją następną sztuczkę wyciągnę {} z {}.",
    "B077": "Krok 1: {}. Krok 2: {}. Krok 3: Profity.",
    "B078": "Jak byłem na kwasie, to {} zmienił się w {}.",
    "B079": "W świecie zniszczonych przez {} naszą jedyną nadzieją będzie {}.",
    "B080": "W nowym filmie akcji Bruce Willis odkrywa, że {} był tak naprawdę {}.",
    "B081": "Nigdy nie rozumiałem {} dopóki nie {}.",
    "B082": "Plotka głosi, że ulubionym daniem Wladimira Putina jest {} nadziewane {}.",
    "B083": "TVP Kultura przedstawia film dokumentalny pt. {} nieznana historia {}.",
    "B084": "{} + {} = {}.",
    "B085": "Stwórz hajku."
}

pick = {
    "B073": 2,
    "B074": 2,
    "B075": 2,
    "B076": 2,
    "B077": 2,
    "B078": 2,
    "B079": 2,
    "B080": 2,
    "B081": 2,
    "B082": 2,
    "B083": 2,
    "B084": 3,
    "B085": 3
}

white_cards_count = len(white_cards)
black_cards_count = len(black_cards)
