from config import *
import socket
import threading
from _thread import *
from uuid import uuid4
from sys import exit
from time import sleep
import random

SERVER_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

CLIENT_LOCK = threading.Lock()
CLIENTS_LOCK = threading.Lock()
WHITE_LOCK = threading.Lock()
NARRATOR_LOCK = threading.Lock()
GAME_LOCK = threading.Lock()

WORKING = True
PLAYING = False
ANSWERS = 0

PLAYERS = {}
CLIENTS = []

NOT_USED_WHITE = [card for card in white_cards.keys()]
NOT_USED_BLACK = [card for card in black_cards.keys()]
USED_WHITE = []
CURRENT_BLACK_CARD = None
CURRENT_WHITE_CARDS = []
THE_CHOSEN_WHITE = {}

EXISTING_NICK_MESSAGE = "AE"
CORRECT_NICK_MESSAGE = "OK {0}"
START_MESSAGE = "START {0} {1} {2} {3} {4}"
START_ROUND_MESSAGE = '{} {}'
NEW_ROUND_MESSAGE = 'NR {}'


# get white card and move it to used pile
def get_white():
    global NOT_USED_WHITE
    # waiting for reshuffle white cards
    while not NOT_USED_WHITE:
        pass
    WHITE_LOCK.acquire()
    white_card = NOT_USED_WHITE[0]
    NOT_USED_WHITE.pop(0)
    WHITE_LOCK.release()
    return white_card


# get ip by connecting to google
def get_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(('www.google.com', 80))
        ip = s.getsockname()[0]
        s.close()
    except socket.error:
        ip = 'localhost'
    finally:
        return ip


def validate_token(token):
    return token in [str(player['token']) for player in list(PLAYERS.values())]


# starting play
def start_play():
    main_play()


def send_points(client):
    client.send("\r\n".join(["{}: {}".format(item[0], item[1]['points']) for item in PLAYERS.items()]).encode('utf-8'))


def main_play():
    global PLAYING
    global CURRENT_BLACK_CARD
    global ANSWERS
    PLAYERS[random.choice(list(PLAYERS.keys()))]['role'] = 'N'
    PLAYING = True
    while NOT_USED_BLACK:
        GAME_LOCK.acquire()
        CLIENTS_LOCK.acquire()
        NARRATOR_LOCK.acquire()
        ANSWERS = 0
        CURRENT_BLACK_CARD = list(pick.keys())[0]
        NOT_USED_BLACK.pop()
        CLIENTS_LOCK.release()
        sleep(30)

        while ANSWERS is not len(PLAYERS) - 1:
            sleep(5)

        NARRATOR_LOCK.release()

        sleep(20)

        NARRATOR_LOCK.acquire()
        CLIENTS_LOCK.acquire()
        for client in CLIENTS:
            send_points(client)
        NARRATOR_LOCK.release()
        CLIENTS_LOCK.release()
        GAME_LOCK.release()
        sleep(5)

    finish()

# finishing server
def finish():
    GAME_LOCK.acquire()
    for client in CLIENTS:
        try:
            client.send("STOP".encode('UTF-8'))
            client.close()
        except socket.error:
            pass
    global PLAYING
    PLAYING = False
    global WORKING
    WORKING = False
    try:
        SERVER_SOCKET.settimeout(1)
        SERVER_SOCKET.close()
    except:
        pass
    exit()


# thread for maintaining console input
def console():
    while WORKING:
        command = input("")
        if command.lower() == 'stop':
            threading.Thread(finish()).start()
        elif command.lower() == 'start':
            threading.Thread(start_new_thread(start_play())).start()


# port validation
def port_valid(port):
    return 1023 < port < 65536


# main thread for client
def client_main(client):
    def client_init():
        nick = client.recv(1024).decode('utf-8')
        while nick in PLAYERS:
            client.send(EXISTING_NICK_MESSAGE.encode('utf-8'))
            nick = client.recv(1024).decode('utf-8')
        print("{} dołączył/a".format(nick))
        PLAYERS[nick] = {'token': uuid4(), 'role': 'P', 'points': 0}
        client.send(str.format(CORRECT_NICK_MESSAGE, PLAYERS[nick]['token']).encode('utf-8'))
        while not PLAYING:
            sleep(1)

        # first cards
        CLIENT_LOCK.acquire()
        PLAYERS[nick]['cards'] = []
        for i in range(0, 5):
            PLAYERS[nick]['cards'].append(get_white())
        CLIENT_LOCK.release()
        client.send(START_MESSAGE.format(*PLAYERS[nick]['cards']).encode('utf-8'))

        return nick

    def client_round():
        sleep(15)
        global ANSWERS
        while CLIENTS_LOCK.locked():
            sleep(5)

        role = PLAYERS[nick]['role']
        client.send(START_ROUND_MESSAGE.format(role, CURRENT_BLACK_CARD).encode('utf-8'))

        if role == 'N':
            client_narrator()
        elif role == 'P':
            client_player()

    def client_narrator():
        global ANSWERS
        while not NARRATOR_LOCK.locked():
            sleep(10)
        NARRATOR_LOCK.acquire()
        while ANSWERS < len(PLAYERS) - 1:
            sleep(5)

        message = "\r\n".join(item[1]['chosen_cards'] for item in THE_CHOSEN_WHITE.items())
        client.send(message.encode('utf-8'))

        data = client.recv(1024).decode('utf-8').split(' ')
        if not validate_token(data[0]):
            print("invalid token {}".format(data[0]))
            finish()
            sleep(120)

        PLAYERS[THE_CHOSEN_WHITE[int(data[1])]['nick']]['points'] += 1
        PLAYERS[THE_CHOSEN_WHITE[int(data[1])]['nick']]['role'] = 'N'
        PLAYERS[nick]['role'] = 'P'

        client.send(NEW_ROUND_MESSAGE.format('').strip().encode('utf-8'))

        NARRATOR_LOCK.release()
        sleep(20)

    def client_player():
        data = client.recv(1024).decode('utf-8').split(' ')
        token = data[0]
        if not validate_token(token):
            finish()
            sleep(120)
        chosen_cards = data[1:]
        CLIENT_LOCK.acquire()
        global ANSWERS
        USED_WHITE.extend(chosen_cards)
        PLAYERS[nick]['cards'] = [card for card in PLAYERS[nick]['cards'] if card not in chosen_cards]
        THE_CHOSEN_WHITE[ANSWERS] = {
            'nick': nick,
            'chosen_cards': " ".join(chosen_cards),
        }
        ANSWERS += 1

        new_cards = []
        for i in chosen_cards:
            new_cards.append(get_white())
        PLAYERS[nick]['cards'].extend(new_cards)
        client.send(NEW_ROUND_MESSAGE.format(" ".join(new_cards)).encode('utf-8'))

        CLIENT_LOCK.release()
        while NARRATOR_LOCK.locked():
            sleep(5)

    nick = client_init()

    while PLAYING:
        client_round()


# init server
def init_server():
    port = int(input('Podaj port nasłuchowy: '))
    while not port_valid(port):
        port = int(input('Port nieprawodiłowy. Podaj port ponownie: '))

    try:
        SERVER_SOCKET.bind((get_ip(), port))
        SERVER_SOCKET.listen(5)

        start_new_thread(console, ())
        while WORKING and not PLAYING:
                client, addr = SERVER_SOCKET.accept()
                if not PLAYING:
                    CLIENTS.append(client)
                    start_new_thread(client_main, (client, ))
        SERVER_SOCKET.close()
    except socket.error:
        if WORKING or PLAYING:
            print('Server socket error')


# Watcher for white cards
def white_card_watcher():
    global USED_WHITE
    global NOT_USED_WHITE
    while True:
        if not NOT_USED_WHITE:
            WHITE_LOCK.acquire()
            NOT_USED_WHITE.extend(USED_WHITE)
            USED_WHITE = []
            NOT_USED_WHITE.shuffle()
            WHITE_LOCK.release()


# real program
init_server()
