import socket
from IPy import IP
from config import *
import ipaddress

my_white_cards = []
playing = True

def ip_valid(s):
    if s == "localhost":
        return True
    else:
        try:
            IP(s)
            return True
        except:
            return False


def port_valid(p):
    if 1023 < int(p) < 65536:
        return True
    else:
        return False


def nick_valid(n):
    if " " not in n and 7 < len(n) < 17:
        print("Nick poprawny.")
        return True
    else:
        print("Niepoprawny nickname. Nickname musi być długości 8-16 znaków oraz ma nie zawierać spacje.")
        return False


def got_role_and_cards(role, black_one, sock):
    global my_white_cards
    if role == "P":
        print("| ROLA |: Zwyky gracz |")
        print(black_cards[black_one].format(*["_____" for i in range(0, pick.get(black_one, 1))]))
        picks = int(pick.get(black_one, 1))
        chosen_cards = []
        while picks > 0:
            white_numbers = [i for i in range(0, picks + 1)]
            for el in white_numbers:
                print(str(el) + ". " + white_cards[my_white_cards[el]])
            num = input("Podaj numer wybranej karty: ")
            if int(num) in white_numbers:
                chosen_cards.append(my_white_cards[int(num)])
                picks -= 1
                white_numbers.remove(int(num))
                del my_white_cards[int(num)]
            else:
                print("Nie możesz wybrać kartę o takim numerze.")
            print("===============================")
        return chosen_cards
    else:
        print("| ROLA |: Narrator |")
        print(black_cards[black_one].format(*["_____" for i in range(0, pick.get(black_one, 1))]))
        print("================ Inni gracze wysyłają odpowiedzi ================")
        chosen_from_others = sock.recv(1024).decode('utf-8')
        i = 0
        chosen_from_others = chosen_from_others.split("\r\n")
        for chosed in chosen_from_others:
            to_print = ""
            for el in chosed.split(' '):
                to_print += " " + white_cards[el] + " ; "
            print(str(i) + ". " + to_print)
            i += 1

        print("================================================================")
        chosed = "-1"
        while int(chosed) < 0 or int(chosed) > len(chosen_from_others):
            chosed = input("Podaj numer najlepszej opcji: ")
        return chosed


def send_clients_choice(s, id_number):
    global my_white_cards
    role_and_black_card = s.recv(20).decode('utf-8').split(" ")
    if role_and_black_card[0] == "P":
        chosen_cards = got_role_and_cards(role_and_black_card[0], role_and_black_card[1], s)
        to_send = id_number
        to_send += " " + " ".join(chosen_cards)
        s.send(to_send.encode('utf-8'))
    elif role_and_black_card[0] == "N":
        the_best = got_role_and_cards(role_and_black_card[0], role_and_black_card[1], s)
        print(the_best)
        s.send((id_number+" "+the_best).encode('utf-8'))
    else:
        global playing
        playing = False
        print("Gra skończona.")


ip = input("Podaj adres IP: ")

if ip_valid(ip):
    port = input("Podaj numer portu: ")
    if port_valid(port):
        # try:
        if type(ipaddress.ip_address(ip)) == "IPv6":
            client_socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        else:
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((ip, int(port)))

        while True:
            nick_name = input("Podaj swój nickname: ")
            while not nick_valid(nick_name):
                nick_name = input("Podaj swój nickname: ")
            client_socket.send(nick_name.encode('utf-8'))

            respond = client_socket.recv(40).decode('utf-8').split(" ")
            if respond[0] == "OK":
                break
            else:
                print("Nickname już istnieje.")

        start_round = client_socket.recv(30).decode('utf-8').split(" ")
        for i in start_round:
            if i in white_cards.keys():
                my_white_cards.append(i)
        send_clients_choice(client_socket, respond[1])

        new_cards = client_socket.recv(30).decode('utf-8').split(" ")
        for i in new_cards:
            if i in white_cards.keys():
                my_white_cards.append(i)

        points = client_socket.recv(4096).decode('utf-8').split("\r\n")
        print("============================ Punkty ============================")
        for pos in points:
            print(pos)

        while True:
            send_clients_choice(client_socket, respond[1])
            if playing:
                new_cards = client_socket.recv(30).decode('utf-8').split(" ")
                for i in new_cards:
                    if i in white_cards.keys():
                        my_white_cards.append(i)
                points = client_socket.recv(4096).decode('utf-8').split("\r\n")
                print("============================ Punkty ============================")
                for pos in points:
                    print(pos)
            else:
                break


        client_socket.close()
    # except socket.error:
    #     print("Błąd przy próbie połączenia.")
    else:
        print("Niepoprawny numer portu. Port musi być w zakresie od 1 do 65535 łącznie.")
else:
    print("Niepoprawny adres IP.")
